CC=gcc
CFLAGS=-x c -E -P

SOURCES:=$(wildcard *.tmpl)
INCLUDES:=$(wildcard *.incl)
OBJS:=$(patsubst %.tmpl, %.html, $(SOURCES))

all: $(OBJS)

%.html: %.tmpl $(INCLUDES)
	$(CC) $(CFLAGS) -o $@ $<

.PHONY: clean
clean:
	rm -f *.html
